<!--
SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>

SPDX-License-Identifier: CC0-1.0
-->

# Aspects of modern Cpp for HPC


## About this course 

This course is part of the annual [HiPerCH workshop](https://www.hkhlr.de/en/events/hiperch-14-2022-09-12) offered by the Competence Center for High Performance Computing in Hessen ([HKHLR](https://www.hkhlr.de/)). For a detailed course description please refer to the [course website](https://www.hkhlr.de/en/articles/hiperch-14-module-2).

## Getting the course material

The course material can be obtained by cloning this repository. E.g. from within a terminal emulator of your choice execute:

```shell
git clone https://git.rwth-aachen.de/hkhlr/aspects-of-modern-cpp-for-hpc.git
```

Please note that you need to have Git installed. This can e.g. be done via `sudo apt-get install git` on Debian-based Linux distros or `brew install git` on macOS with [Homebrew](https://brew.sh/) installed. Git for Windows can be obtained [here](https://git-scm.com/downloads).

## Course material

The course content is comprised of slides (in PDF format) as well as source code examples.

* Slides are located in the [slides](./slides) directory.
* Code examples are located in the [code](./code) directory.

### Code examples

For details on the code examples please refer to this [README](./code/README.md) file.

## Licensing

Please refer to the [LICENSE](./LICENSE.md) file for details on the licenses at use.