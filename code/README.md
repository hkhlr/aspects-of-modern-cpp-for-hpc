<!--
SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>

SPDX-License-Identifier: CC0-1.0
-->

# Code examples

## Relation to the slides

We provide several code examples that are used to illustrate several aspects of C++ mentioned during the lecture.

The directories containing the source code are named to match the names of the slides in which they are referenced:

* [`02-UpdateToCXX`](../slides/HiPerCH22_02-UpdateToCxx.slides.pdf)
* [`03-SOLID`](../slides/HiPerCH22_03-SOLID.slides.pdf)
* [`04-STL`](../slides/HiPerCH22_04-STL.slides.pdf)

## Building the example code

We use CMake as our primary build system generator. You will also need a C++20 enabled compiler. The following GCC versions have been tested under Debian Linux:

* GCC v10.4
* GCC v11.3

### Notes 

* We have only tested building the sources in a *Linux* environment. When you are on Windows we recommend using WSL with a recent compiler. On macOS *most* examples will compile as well but you might have to install a recent GCC compiler version.

* One of the examples uses the NUMA library as well as its header files. When on a Debian-based distro you can install the development version of this library with the command: `apt-get install libnuma-dev` (you might need to execute with the `sudo` command).

### Steps for building

In order to compile all provided source files open a terminal emulator of your choice and and execute the following steps:

1. Clone the [Google Benchmark](https://github.com/google/benchmark) repository and store it in the directory `code/External/google-benchmark` (relative to the root directory of the repository). If you choose a different directory please make sure to update the line 

    ```cmake
    add_subdirectory( External/google-benchmark )
    ```

    in the [CMakeLists.txt](./CMakeLists.txt) file. However, please make sure to place the repository a subdirectory of the `code` directory; otherwise CMake will complain. 

2. Generate the build system:
    ```shell
    $ cmake -B cmake-build -G "Unix Makefiles" \
       -DCMAKE_BUILD_TYPE=Release \
       -DBENCHMARK_ENABLE_TESTING=OFF \
       -DBENCHMARK_DOWNLOAD_DEPENDENCIES=on \
       -DRUN_HAVE_GNU_POSIX_REGEX=0
    ```

3. Build the executables from the source code:
    ```shell
    $ cmake --build cmake-build
    ```

The `cmake-build` directory will mirror the directory structure of the source files. That is, the `cmake-build/03-STL` directory will contain binaries that have been built from the `.cpp` files contained in the `03-STL` directory.