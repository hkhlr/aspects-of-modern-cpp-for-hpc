// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <functional>
#include <iostream>
#include <memory>
#include <random>
#include <vector>

class Sampler
{
public:
    using storage_type = std::vector<double>;
    using size_type = typename storage_type::size_type;
    using value_type = typename storage_type::value_type;

    Sampler() = default;
    Sampler(size_type N)
        : sampling_points_(N)
    {
    }

    virtual ~Sampler() noexcept = default;

    size_type size() const noexcept
    {
        return std::size(sampling_points_);
    }

    value_type at(size_type idx) const
    {
        return sampling_points_.at(idx);
    }

    void resample(value_type xmin, value_type xmax, size_type N)
    {
        makeSampling(sampling_points_, xmin, xmax, N);
    }
    void resample(size_type N)
    {
        value_type const xmin = sampling_points_.at(0UL);
        value_type const xmax = sampling_points_.at(this->size() - 1UL);
        resample(xmin, xmax, N);
    }

private:
    storage_type sampling_points_{};
    virtual void makeSampling(storage_type &data,
                              value_type xmin, value_type xmax, size_type N) = 0;
};

class UniformSampler final : public Sampler
{
public:
    UniformSampler() = default;
    UniformSampler(value_type xmin, value_type xmax, size_type N)
        : Sampler(N)
    {
        resample(xmin, xmax, N);
    }
    ~UniformSampler() noexcept = default;

private:
    void makeSampling(storage_type &data,
                      value_type xmin, value_type xmax, size_type N) override
    {
        data.resize(N);
        value_type const step = (xmax - xmin) / (N - 1.0);
        std::generate(std::begin(data), std::end(data),
                      [&xmin, &step, n = 0]() mutable
                      { return xmin + (n++) * step; });
    }

    storage_type sampling_points_{};
};

class RandomSampler final : public Sampler
{
public:
    RandomSampler() = default;
    RandomSampler(value_type xmin, value_type xmax, size_type N)
        : Sampler(N)
    {
        resample(xmin, xmax, N);
    }
    ~RandomSampler() noexcept = default;

private:
    void makeSampling(storage_type &data,
                      value_type xmin, value_type xmax, size_type N) override
    {
        std::random_device rand_device;
        std::mt19937 gen(rand_device());
        std::uniform_real_distribution distr{xmin, xmax};

        data.resize(N);

        data.at(0UL) = xmin;
        data.at(N - 1UL) = xmax;

        std::generate(std::begin(data) + 1, std::begin(data) + N - 1,
                      [&gen, &distr]()
                      { return distr(gen); });
    }
};

class SampledFunction
{
public:
    using function_type = std::function<double(double)>;
    using value_type = typename function_type::result_type;

    SampledFunction() = default;
    SampledFunction(function_type f, Sampler *s)
        : f_{std::move(f)},
          sampler_{s}
    {
    }

    value_type operator()(int idx) const
    {
        return f_(sampler_->at(idx));
    }

    Sampler &sampler()
    {
        return *sampler_;
    }
    Sampler const &sampler() const
    {
        return *sampler_;
    }

private:
    function_type f_{};
    std::unique_ptr<Sampler> sampler_{};
};

int main()
{
    {
        std::cout << ":: UniformSampler\n";
        auto const f = SampledFunction{
            [](double x)
            { return x * x; },
            new UniformSampler{0.0, 1.0, 5UL},
        };

        for (auto idx = 0UL; idx < f.sampler().size(); ++idx)
        {
            std::cout << f.sampler().at(idx) << " : " << f(idx) << "\n";
        }
    }

    {
        std::cout << ":: RandomSampler\n";
        auto const f = SampledFunction{
            [](double x)
            { return x * x; },
            new RandomSampler{0.0, 1.0, 5UL},
        };

        for (auto idx = 0UL; idx < f.sampler().size(); ++idx)
        {
            std::cout << f.sampler().at(idx) << " : " << f(idx) << "\n";
        }
    }

    return EXIT_SUCCESS;
}