// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

//#include "01_SampledFunction.h"

#include <algorithm>
#include <cmath>
#include <cassert>
#include <iostream>
#include <functional>
#include <random>
#include <stdexcept>
#include <vector>

enum class SamplingType
{
	Uniform,
	Random
};

enum class IntegrationRule
{
	LeftRectangle,
	RightRectangle,
	Trapezoidal,
	Random
};

// The method used for differentiation only affects those
// points that are not at the boundary of the sampling interval.
enum class DifferentiationMethod
{
	ForwardDifferences,
	BackwardDifferences,
	CentralDifferences
};

class SampledFunction
{
public:
	using storage_type = typename std::vector<double>;
	using size_type = typename storage_type::size_type;
	using value_type = typename storage_type::value_type;
	using reference = typename storage_type::reference;
	using const_reference = typename storage_type::const_reference;
	using function_type = std::function<double(double)>;

	SampledFunction() = default;
	explicit SampledFunction(function_type f, value_type xmin, value_type xmax, size_type N,
							 SamplingType sampling_type,
							 IntegrationRule integ_rule,
							 DifferentiationMethod diff_method);

	~SampledFunction() noexcept = default;

	size_type numSamplingPoints() const noexcept;
	storage_type const &samplingPoints() const noexcept;

	value_type at(size_type idx) const;

	void resample(value_type xmin, value_type xmax, value_type step);
	void resample(value_type xmin, value_type xmax, size_type N);

	value_type differentiate(size_type idx) const;
	value_type integrate() const;

private:
	function_type f_{};
	storage_type sampling_points_{};
	SamplingType sampling_type_{};
	IntegrationRule integration_rule_{};
	DifferentiationMethod differentiation_method_{};
};

namespace details
{
	/*
	 * Function to compute the set of points in the sampling interval [ xmin, xmax ]. Points are spaced
	 * by a fixed step size.
	 */
	static void makeUniformSamplingPoints(SampledFunction::storage_type &sampling_points,
										  SampledFunction::value_type xmin,
										  SampledFunction::value_type step)
	{
		std::generate(std::begin(sampling_points), std::end(sampling_points),
					  [&xmin, &step, n = 0UL]() mutable
					  { return xmin + (n++) * step; });
	}

	/*
	 * Function to sample the the interval [ xmin, xmax ] with random points.
	 * The interval contains xmin as its first and xmax as its last point. All
	 * values in between these boudary points will be randomly chosen from
	 * a uniform real distribution.
	 */
	static void makeRandomSamplingPoints(SampledFunction::storage_type &sampling_points,
										 SampledFunction::value_type xmin,
										 SampledFunction::value_type xmax,
										 SampledFunction::size_type N)
	{
		std::random_device rand_device;
		std::mt19937 gen(rand_device());
		std::uniform_real_distribution distr{xmin, xmax};

		sampling_points.resize(N);
		// Make the points at the boundary part of the interval.
		sampling_points.at(0ul) = xmin;
		sampling_points.at(N - 1UL) = xmax;

		// All points except those at the boundary are randomly chosen.
		std::generate(std::begin(sampling_points) + 1, std::begin(sampling_points) + N - 1,
					  [&gen, &distr]()
					  { return distr(gen); });
	}
}

// Ctors
SampledFunction::SampledFunction(function_type f,
								 value_type xmin,
								 value_type xmax,
								 size_type N,
								 SamplingType sampling_type = SamplingType::Uniform,
								 IntegrationRule integ_rule = IntegrationRule::Trapezoidal,
								 DifferentiationMethod diff_method = DifferentiationMethod::CentralDifferences)
	: f_{std::move(f)},
	  sampling_points_(N),
	  sampling_type_{sampling_type},
	  integration_rule_{integ_rule},
	  differentiation_method_{diff_method}
{
	if (sampling_type_ == SamplingType::Uniform)
		details::makeUniformSamplingPoints(sampling_points_, xmin, (xmax - xmin) / (N - 1.0));
	else if (sampling_type_ == SamplingType::Random)
		details::makeRandomSamplingPoints(sampling_points_, xmin, xmax, N);
}

// Getters
SampledFunction::size_type SampledFunction::numSamplingPoints() const noexcept
{
	return std::size(sampling_points_);
}

SampledFunction::storage_type const &SampledFunction::samplingPoints() const noexcept
{
	return sampling_points_;
}

SampledFunction::value_type SampledFunction::at(size_type idx) const
{
	value_type const x = sampling_points_.at(idx);
	return f_(x);
}

// Setters
void SampledFunction::resample(value_type xmin, value_type xmax, value_type step)
{
	size_type const N = std::ceil((xmax - xmin) / step + 1UL);
	sampling_points_.resize(N);
	details::makeUniformSamplingPoints(sampling_points_, xmin, step);
}

void SampledFunction::resample(value_type xmin, value_type xmax, size_type N)
{
	resample(xmin, xmax, (xmax - xmin) / (N - 1.0));
}

// Operate
SampledFunction::value_type SampledFunction::differentiate(size_type idx) const
{
	size_type const N = std::size(sampling_points_);
	assert(N > 1UL && idx < N);

	value_type const step = sampling_points_.at(1UL) - sampling_points_.at(0UL);

	if (this->sampling_type_ == SamplingType::Uniform)
	{
		if (idx == 0UL) // left interval boundary - forward differences
			return (this->at(idx + 1UL) - this->at(idx)) / step;
		else if (idx == (N - 1UL)) // right interval boundary - backward difference
			return (this->at(idx) - this->at(idx - 1UL)) / step;
		else // not at the interval boundaries - central differences
		{
			if (differentiation_method_ == DifferentiationMethod::CentralDifferences)
				return (this->at(idx + 1UL) - this->at(idx - 1UL)) / (2.0 * step);
			else if (differentiation_method_ == DifferentiationMethod::ForwardDifferences)
				return (this->at(idx + 1UL) - this->at(idx)) / step;
			else if (differentiation_method_ == DifferentiationMethod::BackwardDifferences)
				return (this->at(idx) - this->at(idx - 1UL)) / step;
		}
	}
	else
	{
		throw std::runtime_error("Cannot compute derivatives with random sampling.");
	}
}

SampledFunction::value_type SampledFunction::integrate() const
{
	size_type const N = std::size(sampling_points_);
	assert(N >= 2UL); // Required for trapezoidal rule for numerical integration

	// Helper function to avoid lots of code repetition below.
	auto sumValuesFromTo = [this](size_type from, size_type to) -> value_type
	{
		value_type result = 0;
		for (size_type idx = from; idx <= to; ++idx)
			result += this->at(idx);
		return result;
	};

	value_type result = 0;

	if (this->sampling_type_ == SamplingType::Uniform)
	{
		value_type const step = this->sampling_points_.at(1UL) - this->sampling_points_.at(0UL);
		if (this->integration_rule_ == IntegrationRule::Trapezoidal)
		{
			result = (sumValuesFromTo(1UL, N - 2UL) + 0.5 * (this->at(0ul) + this->at(N - 1))) * step;
		}
		else if (this->integration_rule_ == IntegrationRule::LeftRectangle)
		{
			result = sumValuesFromTo(0UL, N - 2UL) * step;
		}
		else if (this->integration_rule_ == IntegrationRule::RightRectangle)
		{
			result = sumValuesFromTo(1UL, N - 1UL) * step;
		}
	}
	else if (this->sampling_type_ == SamplingType::Random)
	{
		value_type const xmin = this->sampling_points_.at(0UL);
		value_type const xmax = this->sampling_points_.at(N - 1UL);
		result = (xmax - xmin) / (N - 1.0) * sumValuesFromTo(0UL, N - 1UL);
	}

	return result;
}

namespace
{
	constexpr double kXMin = 0;
	constexpr double kXMax = 1;
	constexpr std::size_t kNumSamplingPoints = 101UL;

	static double f_square(double x)
	{
		return x * x;
	}
}

void testIntegrateTrapezoidalRule()
{
	auto const f = SampledFunction{
		f_square,
		kXMin,
		kXMax,
		kNumSamplingPoints,
	};

	std::cout << "Result of integration with trapezoidal rule = " << f.integrate() << "\n";
}

void testIntegrateLeftRectangleRule()
{
	auto const f = SampledFunction{
		f_square,
		kXMin,
		kXMax,
		kNumSamplingPoints,
		SamplingType::Uniform,
		IntegrationRule::LeftRectangle,
	};

	std::cout << "Result of integration with left rectangle rule = " << f.integrate() << "\n";
}

void testIntegrateRightRectangleRule()
{
	auto const f = SampledFunction{
		f_square,
		kXMin,
		kXMax,
		kNumSamplingPoints,
		SamplingType::Uniform,
		IntegrationRule::RightRectangle,
	};

	std::cout << "Result of integration with right rectangle rule = " << f.integrate() << "\n";
}

void testIntegrateRandomRule()
{
	auto const f = SampledFunction{
		f_square,
		kXMin,
		kXMax,
		kNumSamplingPoints,
		SamplingType::Random,
	};

	std::cout << "Result of integration with random rule = " << f.integrate() << "\n";
}

int main()
{
	{
		testIntegrateTrapezoidalRule();
		testIntegrateLeftRectangleRule();
		testIntegrateRightRectangleRule();
		testIntegrateRandomRule();
	}

	return EXIT_SUCCESS;
}
