// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <random>
#include <type_traits>
#include <vector>

/*
 * The basic idea for making the interface of the SampledFunction leaner and
 * easier to maintain is to introduce abstractions.
 *
 * In this case we limit ourselves to sampling. We could argue that the function
 * does not really care about the values for which it is supposed to return the
 * values.
 *
 * Therefore, we introduce a dedicated class to manage the process of sampling
 * a particular interval at which we want to obtain the function values.
 * In order to be able to have a unique way of handling a particular "type"
 * of sampler we provide a base class that defined the interface for *all*
 * types of samplers. None of the methods comprising the interface is virtual -
 * this is called the the "Non-virtual Interface Idiom" (NVI). The advantage
 * of using this approach is that we have a unique place in which we define
 * the interface for our samplers. The task of providing the details of
 * sampling (e.g. constant step size, randomly chosen step size) if "offloaded"
 * to the derived classes. They are not concerned with defining (parts) of
 * the interface.
 */
class Sampler
{
public:
	using storage_type = std::vector<double>;
	using value_type = typename storage_type::value_type;
	using size_type = typename storage_type::size_type;

	Sampler() = default;
	Sampler(size_type N)
		: sampling_points_(N)
	{
	}

	virtual ~Sampler() noexcept = default;

	size_type size() const noexcept
	{
		return this->sampling_points_.size();
	}

	value_type at(size_type idx) const
	{
		return sampling_points_.at(idx);
	}

	void resample(value_type xmin, value_type xmax, size_type N)
	{
		makeSampling(sampling_points_, xmin, xmax, N);
	}
	void resample(size_type N)
	{
		value_type const xmin = this->at(0UL);
		value_type const xmax = this->at(this->size() - 1UL);
		this->resample(xmin, xmax, N);
	}

private:
	virtual void makeSampling(storage_type &sampling_points,
							  value_type xmin, value_type xmax, size_type N) = 0;
	storage_type sampling_points_{};
};

class UniformSampler final : public Sampler
{
public:
	UniformSampler() = default;
	UniformSampler(value_type xmin, value_type xmax, size_type N)
		: Sampler{N}
	{
		this->resample(xmin, xmax, N);
	}
	~UniformSampler() noexcept override = default;

private:
	void makeSampling(storage_type &sampling_points,
					  value_type xmin, value_type xmax, size_type N) override
	{
		sampling_points.resize(N);
		value_type const step = (xmax - xmin) / (N - 1.0);
		std::generate(std::begin(sampling_points), std::end(sampling_points),
					  [&step, &xmin, n = 0UL]() mutable
					  { return xmin + (n++) * step; });
	}
};

class RandomSampler final : public Sampler
{
public:
	RandomSampler() = default;
	RandomSampler(value_type xmin, value_type xmax, size_type N)
		: Sampler{N}
	{
		this->resample(xmin, xmax, N);
	}
	~RandomSampler() noexcept override = default;

private:
	void makeSampling(storage_type &sampling_points,
					  value_type xmin, value_type xmax, size_type N) override
	{
		std::random_device rand_device;
		std::mt19937 gen(rand_device());
		std::uniform_real_distribution distr{xmin, xmax};

		sampling_points.resize(N);

		sampling_points.at(0UL) = xmin;
		sampling_points.at(N - 1UL) = xmax;

		std::generate(std::begin(sampling_points) + 1, std::begin(sampling_points) + N - 1,
					  [&distr, &gen]()
					  { return distr(gen); });
	}
};

template <typename SamplerType>
// To make sure we are actually getting a real "Sampler" here!
requires std::is_base_of_v<Sampler, std::decay_t<SamplerType>>
class SampledFunction
{
public:
	using value_type = typename std::decay_t<SamplerType>::value_type;
	using size_type = typename std::decay_t<SamplerType>::size_type;
	using function_type = std::function<value_type(value_type)>;

	SampledFunction() = default;
	SampledFunction(function_type f, SamplerType sampler)
		: f_{std::move(f)}, sampler_{std::move(sampler)}
	{
	}
	~SampledFunction() noexcept = default;

	value_type at(size_type idx) const
	{
		return f_(sampler_.at(idx));
	}
	value_type operator()(size_type idx) const
	{
		return this->at(idx);
	}

	SamplerType const &sampler() const
	{
		return sampler_;
	}
	SamplerType &sampler()
	{
		return sampler_;
	}

private:
	std::function<double(double)> f_{};
	SamplerType sampler_{};
};

namespace
{
	double fdummy(double x)
	{
		return x * x;
	}
}

void testFunctionValue()
{
	std::cout << ":: Function Values\n";
	{
		auto const f = SampledFunction{
			fdummy,
			UniformSampler{0.0, 1.0, 5UL},
		};

		std::cout << ":: Sampling values and corresponding function for uniform sampling:\n";
		for (auto idx = 0UL; idx < f.sampler().size(); ++idx)
		{
			std::cout << f.sampler().at(idx) << ", " << f.at(idx) << "\n";
		}
	}
	{
		std::cout << ":: Sampling values and corresponding function values for random sampling:\n";
		auto const f = SampledFunction{
			fdummy,
			RandomSampler{0.0, 1.0, 5UL},
		};

		for (auto idx = 0UL; idx < f.sampler().size(); ++idx)
		{
			std::cout << f.sampler().at(idx) << ", " << f.at(idx) << "\n";
		}
	}
}

void testResample()
{
	constexpr int initial_size = 101UL;
	constexpr int new_size = 501UL;

	std::cout << ":: Resampling of The Interval\n";
	{
		std::cout << ":: Assert sizes after resampling for uniform sampling\n";
		auto f = SampledFunction{
			fdummy,
			UniformSampler{0.0, 1.0, initial_size},
		};

		assert(f.sampler().size() == initial_size);

		f.sampler().resample(new_size);

		assert(f.sampler().size() == new_size);
	}

	{
		std::cout << ":: Assert sizes after resampling for random sampling\n";
		auto f = SampledFunction{
			fdummy,
			RandomSampler{0.0, 1.0, initial_size},
		};

		assert(f.sampler().size() == initial_size);

		f.sampler().resample(new_size);

		assert(f.sampler().size() == new_size);
	}
}

int main()
{
	testFunctionValue();
	testResample();
	return EXIT_SUCCESS;
}
