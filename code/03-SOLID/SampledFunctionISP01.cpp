// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <functional>
#include <iostream>
#include <vector>

class UniformSampler
{
public:
    using storage_type = std::vector<double>;
    using size_type = typename storage_type::size_type;
    using value_type = typename storage_type::value_type;

    UniformSampler() = default;
    UniformSampler(value_type xmin, value_type xmax, size_type N)
        : sampling_points_(N)
    {
        resample(xmin, xmax, N);
    }

    size_type size() const noexcept
    {
        return std::size(sampling_points_);
    }

    value_type at(size_type idx) const
    {
        return sampling_points_.at(idx);
    }

    void resample(value_type xmin, value_type xmax, size_type N)
    {
        makeSampling(sampling_points_, xmin, xmax, N);
    }
    void resample(size_type N)
    {
        value_type const xmin = sampling_points_.at(0UL);
        value_type const xmax = sampling_points_.at(this->size() - 1UL);
        resample(xmin, xmax, N);
    }

private:
    void makeSampling(storage_type &data, value_type xmin, value_type xmax, size_type N)
    {
        data.resize(N);
        value_type const step = (xmax - xmin) / (N - 1.0);
        std::generate(std::begin(data), std::end(data),
                      [&xmin, &step, n = 0]() mutable
                      { return xmin + (n++) * step; });
    }

    storage_type sampling_points_{};
};

class SampledFunction
{
public:
    using function_type = std::function<double(double)>;
    using value_type = typename function_type::result_type;

    SampledFunction() = default;
    SampledFunction(function_type f, UniformSampler s)
        : f_{std::move(f)}, sampler_{std::move(s)}
    {
    }

    value_type operator()(int idx) const
    {
        return f_(sampler_.at(idx));
    }

    UniformSampler &sampler()
    {
        return sampler_;
    }
    UniformSampler const &sampler() const
    {
        return sampler_;
    }

private:
    function_type f_{};
    UniformSampler sampler_{};
};

int main()
{
    auto const f = SampledFunction{
        [](double x)
        { return x * x; },
        UniformSampler{0.0, 1.0, 5UL},
    };

    for (auto idx = 0UL; idx < f.sampler().size(); ++idx)
    {
        std::cout << f.sampler().at(idx) << " : " << f(idx) << "\n";
    }

    return EXIT_SUCCESS;
}