// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <iostream>
class Class1 {
    public:
    virtual void hello(){
        std::cout << "Class1\n";}
};
class Class2:public Class1{
    public:
        void hello() override {
        std::cout <<"Class2\n";}
};
void callHello(Class1 & anything){
	anything.hello();
};
int main(int argc, char** argv){
    Class1 one=Class1();
    Class2 two=Class2();
    Class1 copy=two;
    Class1& ref1=one;
    ref1=two;
    Class1& ref2=two;
    copy.hello();
    ref1.hello();
    ref2.hello();
    callHello(one);
    callHello(two);
    return 0;
}
