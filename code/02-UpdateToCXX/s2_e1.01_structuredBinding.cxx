// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <utility>
#include <string> 
#include <iostream>
#include <tuple>
struct ReturnWrapper{
	int r1;
	double r2;
};
ReturnWrapper someFunction(int i,double d){
	ReturnWrapper r;
	r.r1=i;
	r.r2=d;
	return r;
}

int main()
{
  int data1; double data2;
  ReturnWrapper ret=someFunction( 100, 12.345 );
  data1=ret.r1;
  data2=ret.r2;
}
