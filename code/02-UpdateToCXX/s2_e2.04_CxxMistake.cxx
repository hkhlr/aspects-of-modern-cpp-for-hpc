// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <iostream>
class Class1 {
  public:
    Class1(){
      std::cout <<"\tctor Class1\t" << this<< std::endl;
    };
	    Class1 & operator=(const Class1 & source){	    
		    std::cout << "\toperator=(const Class1 &)\t" << this<< std::endl;
		    return *this;    
	    }
	    Class1(const Class1 & source){
	    	std::cout << "\tcopyctor Class1\t" << this<< std::endl;
    }
	Class1(Class1 && rhs) {
		std::cout << "\tmovector Class1\t" << this<< std::endl;
	};
	Class1&operator=(Class1 && rhs){
		std::cout << "\toperator(Class1 &&)\t" << this<< std::endl;
		return *this;
	}
	virtual ~Class1(){
		   std::cout << "\tdtor Class1\t" << this<< std::endl;
	   }    
	virtual void hello(){std::cout << "Class1"<<std::endl;;}

};
class Class2:public Class1{
    public:
    Class2(){
      std::cout <<"\tctor Class2\t" << this<< std::endl;
    };
	Class2 & operator=(const Class2 & source){	    
		    std::cout << "\toperator= Class2\t" << this<< std::endl;
		    return *this;    
	    }
	Class2(const Class2 & source){
	    	std::cout << "\tcopyctor Class2\t" << this<< std::endl;;
    }
	Class2(Class2 && rhs) {
		std::cout << "\tmovector Class2\t" << this<< std::endl;
	};
	Class2&operator=(Class2 && rhs){
		std::cout << "\toperator(Class2 &&)\t" << this<< std::endl;
		return *this;
	}
	virtual ~Class2(){
		   std::cout << "\tdtor Class2\t" << this<< std::endl;
	   }
        void hello() override {
        std::cout <<"Class2\n";}
};
void callHello(Class1 & anything){
	anything.hello();
};
int main(int argc, char** argv){
    Class1 one=Class1();
    Class2 two=Class2();
    Class1 copy=two;
    copy.hello();
    Class1& ref1=one;
    ref1.hello();
    ref1=two;
    ref1.hello();
    Class1& ref2=two;
    ref2.hello();
    callHello(one);
    callHello(two);
    one=Class1();
    return 0;
}
