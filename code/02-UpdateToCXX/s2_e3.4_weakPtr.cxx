// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <memory>
#include <iostream>
using namespace std;
struct Example {
	int myInt;
	Example(int arg):myInt(arg){};
	virtual ~Example(){cout<<"~Example" << myInt<<endl;};
};

int main(int argc,char**){
  weak_ptr<Example> aRef;
  {
  	shared_ptr<Example> a=make_shared<Example>(1);
	aRef=a;
	cout << "Available references to a " << aRef.use_count() << endl;
	cout << "Use of aRef" << aRef.lock()->myInt<<endl;
  }
  cout << "Available references to a " << aRef.use_count() << endl;
  auto aNewSharedPtr=aRef.lock();
  if (aNewSharedPtr!=nullptr) {
    cout << "Use of aRef" <<endl;
  } else {
    cout << "aRef was deconstructed" << std::endl;
  }
  return 0;
}
