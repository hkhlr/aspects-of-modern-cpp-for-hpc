# SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
#
# SPDX-License-Identifier: CC0-1.0

find_package( OpenMP REQUIRED )

file( GLOB ALL_CXX_SOURCES ./*.cxx )

foreach(
        cppsource
	${ALL_CXX_SOURCES}
)
    get_filename_component( exename ${cppsource} NAME_WLE )
    add_executable(
            ${exename}
            ${cppsource}
    )
endforeach()

target_link_libraries(
	s2_e3.5_NUMA PRIVATE
	numa
	OpenMP::OpenMP_CXX 
)
	

