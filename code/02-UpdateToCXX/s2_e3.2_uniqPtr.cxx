// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <memory>
#include <iostream>
using namespace std;
struct Example {
  int myInt;
  Example(int arg):myInt(arg){};
  virtual ~Example(){cout<<"~Example" << myInt<<endl;};
};
struct Adapter {
  unique_ptr<Example> TheExample;
  Adapter(unique_ptr<Example> ptr):TheExample(move(ptr)){};
  Example * get(){return TheExample.get();}

};

int main(int argc,char**){
  Example * rawPtr = new Example(11);
  cout << "rawPtr = " << rawPtr<< endl;
  Adapter adapter=Adapter(unique_ptr<Example>(rawPtr));
  cout << "rawPtr = " << rawPtr<< endl;
  cout << "Owned Example:" << adapter.get()->myInt << endl;
  adapter=Adapter(nullptr);
  cout << "End of Program" << endl;
  return 0;
}
