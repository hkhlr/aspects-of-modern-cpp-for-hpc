// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <utility>
#include <string> 
#include <iostream>
#include <tuple>
class MyClass{
	public:
		std::string name;
		int index;
		double  value;
		MyClass():name("Empty"),index(0),value(3.141){};
		MyClass(std::string arg1,int arg2,int arg3):name(arg1),index(arg2),value(arg3){};
};
int main(...){
	MyClass a;
	auto && [name1,index1,value1] = a;
	return 0;
}
