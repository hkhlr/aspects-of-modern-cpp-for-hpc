// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <memory>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <algorithm>
#include <numaif.h>
#include <unistd.h>
#include <cstddef>

template <class TP> class  NumaAllocator {
	public:
		typedef TP value_type;
		NumaAllocator()=default;
		template <class T> NumaAllocator(const NumaAllocator<T> &) {}
		TP * allocate(std::size_t n) {
			std::cout << "allocating " << n << " Bytes" << std::endl;
			return static_cast<TP*>(std::malloc(n));
		}
		void deallocate(TP * p,std::size_t n) {
			std::free(p);
		}
};

class AClass {
	protected:
		bool initialized;
		double dataX,dataY,dataZ;
	public:
		AClass(double x,double y, double z):dataX(x),dataY(y),dataZ(z) {}
	       explicit AClass():initialized(true){
		std::cout << "AClass default-ctored"<<std::endl;
	       }
	       AClass(const AClass & c){
		std::cout << "AClass ctored"<<std::endl;
	       }
		void       print() {
		       std::cout << initialized << "(" << dataX << "," << dataY << "," << dataZ<< ")" << std::endl;
	       }
};


int main(...) {
	int numberPages=8;
	long pagesize = sysconf(_SC_PAGE_SIZE);
	std::cout << "PageSize" << pagesize << std::endl;
	std::vector<AClass,NumaAllocator<AClass> > v1;
	v1.reserve(100);
	v1[40].print();
	std::vector<AClass> v2;
	v2.reserve(100);
	v2[40].print();
	std::cout << "V2.size()="<< v2.size() << std::endl;

	std::generate_n(std::back_inserter(v2),10,[](){return AClass(1.0,2.0,3.0);});
#pragma omp parallel for
	for (int i=0;i< 100; i++) {
	//	v2[i](1.0,2.0,3.0);
	}
#pragma omp parallel for
	for(auto &i:v2) {
		i=AClass(1.0,2.0,3.0);
	}
	v2[33].print();
	std::cout << "Default alginment of malloc is " << alignof(std::max_align_t) << std::endl;
	unsigned char * alignedMem =(unsigned char *) aligned_alloc(pagesize,pagesize*numberPages);
	std::cout << "Case #1" << std::endl;
#pragma omp parallel for	
	for (int i=0;i<numberPages;i++)	 {
		alignedMem[pagesize*i]=0;
	}

	int status[1];

	for (int i=0;i<numberPages;i++){
		void * ptr=alignedMem+pagesize*i;
		move_pages(0,1,&ptr,NULL,status,0);
		std::cout << "Adress " << alignedMem+pagesize*i << " located a NUMA-node" << status[0] << std::endl;
	}
	free(alignedMem);

	std::cout << "Case #2" << std::endl;
	alignedMem =(unsigned char *) aligned_alloc(pagesize,pagesize*numberPages);
	for (int i=0;i<numberPages;i++)	 {
		alignedMem[pagesize*i]=0;
	}
	for (int i=0;i<numberPages;i++){
		void * ptr=alignedMem+pagesize*i;
		move_pages(0,1,&ptr,NULL,status,0);
		std::cout << "Adress " << alignedMem+pagesize*i << " located a NUMA-node" << status[0] << std::endl;
	}
}
