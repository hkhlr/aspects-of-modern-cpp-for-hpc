// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <utility>
#include <string> 
#include <iostream>
#include <tuple>

std::tuple<int,double> aFunction(){
	return std::tuple<int,double>(15,1337);
}

int main(...){
	auto && [data1,data2]=aFunction();
	return 0;
}