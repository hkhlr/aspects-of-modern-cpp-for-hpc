// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <utility>
#include <string> 
#include <iostream>
#include <tuple>
class MyClass{
	protected:
	public:
		std::string name;
		int index;
		double  value;
		MyClass():name("Empty"),index(0),value(3.141){};
		template<std::size_t Index> std::tuple_element_t<Index,MyClass> &get() {
			if constexpr (Index == 0) return name;
			if constexpr (Index == 1) return index;
			if constexpr (Index == 2) return value;
		}
		MyClass(std::string arg1,int arg2,int arg3):name(arg1),index(arg2),value(arg3){};
		operator std::tuple<std::string ,int ,double >()  { return std::tuple<std::string,int,double>(name,index,value);};
		operator std::tuple<std::string ,int ,double >() const { return std::tuple<std::string,int,double>(name,index,value);};
			
};

/*
// free function
template<std::size_t Index> std::tuple_element_t<Index,MyClass> & get(MyClass& src){
	if constexpr (Index == 0) return src.name;
	if constexpr (Index == 1) return src.index;
	if constexpr (Index == 2) return src.value;
} */

template<> struct std::tuple_size<MyClass>: integral_constant<size_t,3>{};
template<> struct std::tuple_element<0,MyClass> { using type = std::string;};
template<> struct std::tuple_element<1,MyClass> { using type = int ;};
template<> struct std::tuple_element<2,MyClass> { using type = double;};

std::tuple<std::string,int,double> aFunction(){
	return std::tuple<std::string,int,double>("Hello World",15,1337);
}

int main(...){
	MyClass a,b("instanceB",4,2.71828);
	auto && [name1,index1,value1] = a;
	std::cout << "Case1" << std::endl;
	std::cout << "Line#36: Name =" << name1 << " Index="<<index1 << " Value=" << value1 << std::endl;
	a.name=std::string("Hello");
	std::cout << "Line38: Name =" << name1 << " Index="<<index1 << " Value=" << value1 << std::endl;
	std::cout << "Case2" << std::endl;
	auto [name2,index2,value2] = a;
	std::cout << "Line#41: Name =" << name2 << " Index="<<index2 << " Value=" << value2 << std::endl;
	a.name=std::string("Changed");
	std::cout << "Line#43: Name =" << name2 << " Index="<<index2 << " Value=" << value2 << std::endl;
	auto && [name3,index3,value3]=aFunction();
	std::cout << "Line#45: Name =" << name3 << " Index="<<index3 << " Value=" << value3 << std::endl;
	//std::string name2; int index2; double value2;
	std::tie (name2,std::ignore,value2)=static_cast<std::tuple<std::string,int,double> > (b);
	std::cout << "Line48: Name =" << name2 << " Index="<<index2 << " Value=" << value2 << std::endl;

	return 0;
}
