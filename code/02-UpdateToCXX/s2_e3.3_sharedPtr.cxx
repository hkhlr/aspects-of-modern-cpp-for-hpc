// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <memory>
#include <iostream>
using namespace std;
struct Example {
	int myInt;
	Example(int arg):myInt(arg){};
	virtual ~Example(){cout<<"~Example" << myInt<<endl;};
};

int main(int argc,char**){
  shared_ptr<Example> a=make_shared<Example>(1);
  shared_ptr<Example> b=make_shared<Example>(2);
  cout << "A:" << a.use_count() << endl;
  cout << "B:" << b.use_count() << endl;
  b=a;
  cout << "A:" << a.use_count() << endl;
  cout << "B:" << b.use_count() << endl;
  a=nullptr;
  cout << "A:" << a.use_count() << endl;
  cout << "B:" << b.use_count() << endl;
  return 0;
}
