// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <iostream>
struct Class1 {
virtual void hello(){
std::cout << "Hello Class1\n";
}
};
struct Class2 : public Class1{

    void hello() override {
    std::cout <<"Class2A\n";
    }
};

int main(int argc, char** argv){
auto one=Class1();
auto twoA=Class2();
Class1& ref1 = one;
ref1.hello();
ref1 = twoA;
ref1.hello();
return 0;
};