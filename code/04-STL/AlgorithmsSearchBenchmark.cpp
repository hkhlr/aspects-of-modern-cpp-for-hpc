// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
// SPDX-FileCopyrightText: © 2022 HPC Core Facility of the Justus-Liebig-University Giessen <marcel.giar@physik.jlug.de>
//
// SPDX-License-Identifier: MIT

//
// Created by marcel on 3/8/22.
//

#include <algorithm>
#include <vector>
#include "benchmark/benchmark.h"

static std::vector<int> makeSortedVector( std::size_t size )
{
    std::vector<int> v( size );
    std::generate( std::begin( v ), std::end( v ),
                   [ n = 0 ]() mutable { return ++n; } );
    return v;
}

static bool generalSearch( std::vector<int> const& v, int sought_value )
{
    auto it = std::find( std::cbegin( v ), std::cend( v ), sought_value );
    return ( it != std::cend( v ) );
}

static bool binarySearch( std::vector<int> const& v, int sought_value )
{
    return std::binary_search( std::cbegin( v ), std::cend( v ), sought_value );
}

static void bm_generalSearch( benchmark::State& state )
{
    int const n = state.range( 0 );
    auto v{ makeSortedVector( n ) };
    for ( auto _ : state )
    {
        benchmark::DoNotOptimize( generalSearch( v, n ) );
    }
    state.SetComplexityN( n );
}

static void bm_binarySearch( benchmark::State& state )
{
    int const n = state.range( 0 );
    auto v{ makeSortedVector( n ) };
    for ( auto _ : state )
    {
        benchmark::DoNotOptimize( binarySearch( v, n ) );
    }
    state.SetComplexityN( n );
}

BENCHMARK( bm_generalSearch )->RangeMultiplier( 2 )->Range( 512, 1 << 16 )->Complexity();
BENCHMARK( bm_binarySearch )->RangeMultiplier( 2 )->Range( 512, 1 << 16 )->Complexity();

BENCHMARK_MAIN();