// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
// SPDX-FileCopyrightText: © 2022 HPC Core Facility of the Justus-Liebig-University Giessen <marcel.giar@physik.jlug.de>
//
// SPDX-License-Identifier: MIT

#include <algorithm>
#include <iostream>
#include <numeric>
#include <string_view>
#include <vector>

template <typename InputIt1, typename InputIt2>
void print( std::string_view comment, InputIt1 begin, InputIt2 end )
{
    std::cout << comment << "\n";
    for ( ; begin != end; ++begin ) std::cout << *begin << ' ';
    std::cout << "\n";
}

int main()
{
    constexpr std::size_t kSize{ 20UL };

    std::vector<int> input_buffer( kSize );
    std::vector<int> output_buffer( kSize );

    std::iota( std::begin( input_buffer ), std::end( input_buffer ), 1 );
    print( "\nInput buffer", std::cbegin( input_buffer ), std::cend( input_buffer ) );
    std::cout << "The size of the input_buffer is " << std::size( input_buffer ) << "\n";

    // `copy` returns an output iterators that points to one past-the-end of the last-copied
    // element.
    auto it_end = std::copy( std::cbegin( input_buffer ), std::cend( input_buffer ),
                             std::begin( output_buffer ) );
    print( "\nInput buffer copied to output buffer", std::cbegin( output_buffer ), it_end );
    std::cout << "The size of the output_buffer is " << std::size( input_buffer ) << "\n";
    //    std::fill( std::begin( output_buffer ), std::end( output_buffer ), 0 ); // reset the
    //    buffer

    // `copy_if` returns an output iterators that points to one past-the-end of the last-copied
    // element.
    auto it_end_conditional = std::copy_if(
        std::cbegin( input_buffer ), std::cend( input_buffer ), std::begin( output_buffer ),
        []( auto x ) { return x % 2 == 0; } ); // Predicate for condition
    print( "\nEven elements copied from input buffer", std::begin( output_buffer ),
           it_end_conditional );
    // NOTE: The size of the output_buffer did not change!
    std::cout << "The size of the output_buffer is " << std::size( output_buffer ) << "\n";

    return EXIT_SUCCESS;
}
