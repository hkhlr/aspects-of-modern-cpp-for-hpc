// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
// SPDX-FileCopyrightText: © 2022 HPC Core Facility of the Justus-Liebig-University Giessen <marcel.giar@physik.jlug.de>
//
// SPDX-License-Identifier: MIT

#include <algorithm>
#include <concepts>
#include <iostream>
#include <vector>

int main()
{
    std::vector<int> values( 10UL );
    std::ranges::generate( values, [ n = 0 ]() mutable { return ++n; } );
    std::ranges::for_each( values, []( auto x ) { std::cout << x << " "; } );
}
