// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
//
// SPDX-License-Identifier: MIT

#include <array>
#include <iostream>
#include <span>
#include <string_view>
#include <vector>

template< typename T >
void print( std::string_view comment, std::span<T> array )
{
    std::cout << comment;
    for ( auto idx = 0UL; idx < std::size( array ); ++idx )
    {
        std::cout << array[ idx ] << " ";
    }
    std::cout << "\n";
}

int main()
{
    std::array  array{ 1, 2, 3, 4, 5 };
    print<int>( "Print content of std::array\n", array );

    std::vector vector{ 1, 2, 3, 4, 5 };
    print<int>( "Print content of std::vector=\n", vector );

    int c_array[]{ 1, 2, 3, 4, 5 };
    print<int>( "Printing the content of C-style array\n", c_array );

    return EXIT_SUCCESS;
}
