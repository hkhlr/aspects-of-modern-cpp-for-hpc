// SPDX-FileCopyrightText: © 2022 Competence Center for High Performance Computing in Hessen (HKHLR) <christian.iwainksy@hpc-hessen.de>
// SPDX-FileCopyrightText: © 2022 HPC Core Facility of the Justus-Liebig-University Giessen <marcel.giar@physik.jlug.de>
//
// SPDX-License-Identifier: MIT

#include <algorithm>
#include <cmath>
#include <iostream>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <vector>

template <typename T>
struct NumericFunction
{
    std::vector<T> x_values{}; // Points at which the function is sampled.
    std::vector<T> f_values{}; // Function values at sampling points.
};

struct RiemannSummationType
{
};
struct RiemannSummationTypeRight : RiemannSummationType
{
};
struct RiemannSummationTypeLeft : RiemannSummationType
{
};

// template <typename T>
// void printValues( std::vector<T> const &values )
//{
//     for ( auto const &value : values ) std::cout << value << " ";
//     std::cout << "\n";
// }

template <typename T = double, typename Func>
NumericFunction<T> sampleFunction( std::tuple<T, T, T> sampled_interval, Func f )
{
    auto const &[ lbound, ubound, step_size ] = sampled_interval;
    std::size_t const num_samples = std::ceil( ( ubound - lbound ) / step_size + 1 );

    NumericFunction<T> fn{};
    fn.x_values.resize( num_samples );
    fn.f_values.resize( num_samples );

    // Generate the range of sampling values.
    std::generate( std::begin( fn.x_values ), std::end( fn.x_values ),
                   [ &xmin = lbound, &dx = step_size, n = 0 ]() mutable
                   { return xmin + ( n++ ) * dx; } );
    // Generate the range of functions values at the sampling points.
    std::transform( std::cbegin( fn.x_values ), std::cend( fn.x_values ), std::begin( fn.f_values ),
                    f );
    return fn;
}

template <typename T>
T computeRightRiemannSumSTL( NumericFunction<T> const &fn )
{
    // Get the distances between adjacent points in the sampling interval.
    std::vector<T> steps( std::size( fn.x_values ) );
    std::adjacent_difference( std::cbegin( fn.x_values ), std::cend( fn.x_values ),
                              std::begin( steps ) );
    // Compute the right Riemann sum.
    auto result =
        std::inner_product( std::next( std::cbegin( fn.f_values ) ), std::cend( fn.f_values ),
                            std::next( std::begin( steps ) ), 0.0 );
    return result;
}

template <typename T>
T computeRightRiemannSumReference( NumericFunction<T> const &fn )
{
    auto result{ T( 0 ) };
    for ( std::size_t idx{ 1UL }; idx < std::size( fn.x_values ); ++idx )
    {
        auto const step{ fn.x_values[ idx ] - fn.x_values[ idx - 1UL ] };
        result += fn.f_values[ idx ] * step;
    }

    return result;
}

template <typename T>
T computeLeftRiemannSumReference( NumericFunction<T> const &fn )
{
    auto result{ T( 0 ) };
    for ( std::size_t idx{ 0UL }; idx < std::size( fn.x_values ) - 1UL; ++idx )
    {
        auto const step{ fn.x_values[ idx + 1UL ] - fn.x_values[ idx ] };
        result += fn.f_values[ idx ] * step;
    }

    return result;
}

template <typename SummationType, typename InputIt>
std::enable_if_t<std::is_same_v<SummationType, RiemannSummationTypeRight>,
                 typename std::iterator_traits<InputIt>::value_type>
computeRiemannSumCustom( InputIt first_x, InputIt last_x, InputIt first_f )
{
    using ValueType = typename std::iterator_traits<InputIt>::value_type;

    auto previous_x{ first_x };
    ++first_x;
    ++first_f;
    auto result{ ValueType( 0 ) };

    while ( first_x != last_x )
    {
        auto const step{ *first_x - *previous_x };
        result += *first_f * step;
        ++first_x;
        ++previous_x;
        ++first_f;
    }

    return result;
}

template <typename SummationType, typename InputIt>
std::enable_if_t<std::is_same_v<SummationType, RiemannSummationTypeLeft>,
                 typename std::iterator_traits<InputIt>::value_type>
computeRiemannSumCustom( InputIt first_x, InputIt last_x, InputIt first_f )
{
    using ValueType = typename std::iterator_traits<InputIt>::value_type;

    auto next_x{ std::next( first_x ) };
    auto result{ ValueType( 0 ) };

    // Note how we use `next_x` for the break condition of the loop.
    while ( next_x != last_x )
    {
        auto const step{ *next_x - *first_x };
        result += *first_f * step;
        ++first_x;
        ++next_x;
        ++first_f;
    }

    return result;
}

int main()
{
    // Define interval and step size at which to sample the interval.
    auto const sampled_interval{ std::make_tuple( 0.0, // lower bound
                                                  2.0, // upper bound
                                                  1e-3 // step size
                                                  ) };

    auto const f_numeric{ sampleFunction( sampled_interval, []( auto x ) { return x * x; } ) };

    // Result from left Riemann summation
    auto const result_right{ computeRightRiemannSumSTL( f_numeric ) };
    auto const result_right_custom = computeRiemannSumCustom<RiemannSummationTypeRight>(
        std::cbegin( f_numeric.x_values ), std::cend( f_numeric.x_values ),
        std::cbegin( f_numeric.f_values ) );
    auto const result_right_reference{ computeRightRiemannSumReference( f_numeric ) };

    std::cout << "Result of right Riemann sum with STL  = " << result_right << "\n";
    std::cout << "Result of right Riemann custom sum    = " << result_right_custom << "\n";
    std::cout << "Result of reference right Riemann sum = " << result_right_reference << "\n";

    // Result from right Riemann summation
    auto const result_left_reference{ computeLeftRiemannSumReference( f_numeric ) };
    auto const result_left_custom{ computeRiemannSumCustom<RiemannSummationTypeLeft>(
        std::cbegin( f_numeric.x_values ), std::cend( f_numeric.x_values ),
        std::cbegin( f_numeric.f_values ) ) };

    std::cout << "Result of left Riemann custom sum     = " << result_left_custom << "\n";
    std::cout << "Result of reference left Riemann sum  = " << result_left_reference << "\n";

    return EXIT_SUCCESS;
}
